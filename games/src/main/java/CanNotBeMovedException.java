public class CanNotBeMovedException extends Exception {

    private final String description;

    public CanNotBeMovedException(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
