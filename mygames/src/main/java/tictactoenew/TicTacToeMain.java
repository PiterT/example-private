package tictactoenew;

import java.util.Scanner;

public class TicTacToeMain {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        GameBoard gameBoard = new GameBoard();
        boolean ifSuccess = true;
        String firstPlayer;
        String secondPlayer;
        int isEmptyFields = 0;

        System.out.println("First player's symbol is circle");
        System.out.println("Enter name first player: ");
        firstPlayer = input.nextLine();
        System.out.println("Enter name second player: ");
        secondPlayer = input.nextLine();


        while (!ifSuccess || isEmptyFields < 1) {
            System.out.println("Enter coordiantes: ");
            String userChoice = input.nextLine();

            gameBoard.display();
            try {
                gameBoard.move(new FieldCoordinates(userChoice));

            } catch (Exception e) {
                throw new IllegalStateException("Wrong coordinates");
            }

            isEmptyFields++;


        }


    }


}
